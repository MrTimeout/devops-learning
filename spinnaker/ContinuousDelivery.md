# Why Continuous Delivery?

CD is the practice by which software changes can be deployed to production in a
_fast_, _safe_ and _automatic_ way.

## Benefits

- Innovation: CD ensures quicker time to market for:
  + New Features.
  + Configuration changes.
  + Experiments.
  + bug fixes.
- Faster feedback loops: Samller changes deployed frequently makes it easier to troubleshoot issues.
- Increase reliability and availability: CD encourages the use of automatic workflows. CD pipelines
can further be crafter to incrementally roll out changes at specific times and different cloud targets.
- Developer productivity and efficiency: It helps to developers to solve issues that arises when deploying.

## Example

In Netflix, they use these steps for CI/CD:

- Code check-in
- Continuous Integration
- Bake
- Deployment
- Post-deploy test
- Hotfix
- Canary
- Live
