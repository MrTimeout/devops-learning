# Cloud Deployment Considerations

## Credentials Management

Where and how we store the secret data? That's the question to ask here.

A good option could be a cloud provider _Identity and Access Management_(IAM) service.

## Regional Isolation

Cloud providers tend to organize their infrastructure into addresable _zones_ and _regions_.

A _zone_ is a physical _data center_. Several zones in close proximity make up a _region_.

## Autoscaling

Kubernetes, AWS autoscale self healing and recovering from pods that are destroyed because of failure or outOfMemory.

### CPU Utilization is wrong

When we say that a CPU is _stalled_, it means the processor is not making _forward process_ with instructions,
and usually happens because it is waiting on memory I/O.

The metric we call CPU utilization is really "non-idle time": the time the CPU was not running the idle thread.

If a non-idle thread begins running, then stops 100 milliseconds later, the kernel considers that CPU utilized that entire time.
This is called _Dummy Job_.

Nowadays, CPUs have become faster than main memory, and waiting on memory dominates what is still called "CPU utilization".
the CPU DRAM gap.

We can measure the CPU Instructions Per Cycle (IPC) using the Performance Monitoring Counters(PMCs):

```sh
# perf stat -a -- sleep 10

 Performance counter stats for 'system wide':

     641398.723351      task-clock (msec)         #   64.116 CPUs utilized            (100.00%)
           379,651      context-switches          #    0.592 K/sec                    (100.00%)
            51,546      cpu-migrations            #    0.080 K/sec                    (100.00%)
        13,423,039      page-faults               #    0.021 M/sec                  
 1,433,972,173,374      cycles                    #    2.236 GHz                      (75.02%)
   <not supported>      stalled-cycles-frontend  
   <not supported>      stalled-cycles-backend   
 1,118,336,816,068      instructions              #    0.78  insns per cycle          (75.01%)
   249,644,142,804      branches                  #  389.218 M/sec                    (75.01%)
     7,791,449,769      branch-misses             #    3.12% of all branches          (75.01%)

      10.003794539 seconds time elapsed
```

## Immutable Infraestructure and Data Persistence

Public clouds made the Immutable Server pattern widely accessible for the first time.

The Immutable Server mainly consists in changing the _base image_ instead of the running 
instance of the service. By doing so, we are avoiding the _Snowflake Server_, which means SSHing into
the running instance and change configuration. Instead of following this approach, it is better to use
tools like puppet and chef where we can put all the configuration and test it before updating the server.
More on that, if you don't update the server consistenly (depending on your type of service), we could get 
_configuration drift_, which means that servers that were initially running same base image, now differs.
In that case, the best approach would be to update more often these servers with base-images well tested to ensure
that it doesn't happen. After all, we are talking about _Phoenix Server_.

## Service Discovery

It is how cloud microservices typically find each other across ever-changing topologies.

For example, Kubernetes supports this feature using _Services_ and _Endpoints_.

Amazon's _Application Load Balancer_(ALB) is better suited for mid-tier load balancing than its original
_Elastic Load Balancer_ offering.

## Links

- [CPU](https://www.brendangregg.com/blog/2017-05-09/cpu-utilization-is-wrong.html)
- [USE](https://www.brendangregg.com/usemethod.html)
- [Inmmutable Server Pattern](https://devops.stackexchange.com/questions/412/what-are-immutable-servers)
- [Snowflake Server](https://martinfowler.com/bliki/SnowflakeServer.html)
- [Phoenix Server](https://martinfowler.com/bliki/PhoenixServer.html)
- [Configuration Drift](http://kief.com/configuration-drift.html)
- [Lifecycle of a server](http://kief.com/automated-server-management-lifecycle.html)

## Books to read

- [The visible Ops Handbook](https://www.amazon.com/gp/product/0975568604/ref=as_li_tl?ie=UTF8&camp=1789&creative=9325&creativeASIN=0975568604&linkCode=as2&tag=martinfowlerc-20)
- [Continous Delivery](https://martinfowler.com/books/continuousDelivery.html)
