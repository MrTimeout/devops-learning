#!/usr/bin/env bash

# By default, it will output the variables using space as a OFS
awk -F ':' '{print $1, $NF}' /etc/passwd # This is called FS (Field Separator)
awk 'BEGIN{FS=":";} {print $1, $NF}' /etc/passwd

# We can output using another character different from space.
awk -F ':' 'BEGIN{OFS="\t";} {print $1, $NF}' /etc/passwd
awk 'BEGIN{OFS="\t";FS=":";} {print $1, $NF}' /etc/passwd

# awk by default, read each file or text line by line specifying the Record Separator(RS) to \n
echo $PATH | awk -v RS=: '{print $1;}'
echo $PATH | awk 'BEGIN{RS=":";} {print $1;}'

# ORS (Output Record Separator) Same as OFS but with RS.
echo $PATH | awk -v RS=: -v ORS=: '{print $1;}'
