# Cheatsheet JQ

## Simple objects

```sh
> echo '{"message": "Hello world"}' | jq '.message'
# output -> "Hello world"
```

```sh
> echo '{"message": "Hello world"}' | jq -r '.message'
# output -> Hello world
```

```sh
> echo '{"gretting message": "Hello world"}' | jq '."gretting message"'
# output -> "Hello world"
```

```sh
> echo '[{"value": "first"}, {"value": "second"}]' | jq -r '.[].value'
# output -> first\nsecond\n
```

```sh
> echo '{"a": 1, "b": 3}' | jq '.[]'
# output -> 1\n3\n
```

```sh
> echo '{"a": 1, "b": 3}' | jq -r 'keys | .[]'
# output -> a\nb\n
```

## Creating objects

```sh
> echo '{"a": "first", "b": "second", "c": "third"}' | jq '[('.a', '.b')]'
# output -> [\n"first",\n"second"\n]

> echo '{"a": {"key": "first"}, "b": {"key": "second"}, "c": "third"}' | jq '[('.a', '.b') | keys]'
# output -> [["key"], ["key"]]

> echo '{"a": {"key": "first"}, "b": {"key": "second"}, "c": "third"}' | jq '[('.a', '.b') | keys] | flatten'
# output -> [ "key", "key" ]

> echo '{"a": {"key": "first"}, "b": {"key": "second"}, "c": "third"}' | jq '[('.a', '.b') | keys] | flatten | length'
# output -> 2
```

```sh
> echo '{"this": { "is": { "my": { "username": "MrTimeout" } } } }' | jq -r '{ name: .this.is.my.username }'
# output -> { "name": "MrTimeout" }
```

## Arrays

```sh
> echo '[1,2,3]' | jq '.[1]'
# output -> 2
```

```sh
> echo '[1,2,3]' | jq '.[]'
# output -> 1\n2\n3\n
```

```sh
> echo '[1,2,3]' | jq 'length'
# output -> 3
```
