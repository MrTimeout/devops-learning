#!/bin/bash
# We don't really need chown capability on any container.
# Here we are creating a docker container with the CHOWN capability dropped, so we cannot change the UID or GID of any file on the fs.
docker container run --name chown --rm -it --volume "$PWD/testing:/testing" --cap-drop "CHOWN" alpine:latest /bin/sh -c "cd testing; chown root:root init.sh"

docker container run --name chown --rm -it --volume "$PWD/testing:/testing" --cap-drop ALL --cap-add "CHOWN" alpine:latest /bin/sh -c "cd testing; chown root:root init.sh"
