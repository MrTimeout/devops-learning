#!/bin/bash
# More info: https://www.redhat.com/en/blog/secure-your-containers-one-weird-trick
# DAC means Discretionary Access Control
# This is used by root to bypass read, write and execute permission checks.
# This means that a root capable can read, write and execute any file on the system, even if the permission or ownership fields wouldn't allow it.
# This capability is not needed in any container...
docker container run --name dac -it --rm --volume "$PWD/testing:/testing" --cap-drop DAC_OVERRIDE alpine /bin/sh -c "cd testing; ./init.sh" # This will throw permission denied when executing.

# Here we can see that with only this capacity, we can execute the script of another user being root capable.
docker container run --name dac -it --rm --volume "$PWD/testing:/testing" --cap-drop ALL --cap-add DAC_OVERRIDE alpine /bin/sh -c "cd testing; ./init.sh"
