#!/bin/bash
# help: https://man7.org/linux/man-pages/man1/chattr.1.html
# Here we are going to explain about _chattr_ and _lsattr_ commands.
#
# We can change the attributes of a file in linux using this attributes.
# For some of them, we will need the CAP_LINUX_IMMUTABLE capability.
#
# We can start with the 'a' attribute:
Id=$(docker container run --name chattr --rm -itd --cap-add LINUX_IMMUTABLE ubuntu:22.04 /bin/bash)

docker container exec -it $Id /bin/bash -c "apt-get update && apt-get install --yes nano wget curl && touch /someting.txt"

########### a ###########
# We can modify the file, appending content to it, but we can't rewrite the file or edit with any editor and change its content...
# To set or unset this attribute, we need to be a root capable with the CAP_LINUX_IMMUTABLE.
test $1 -eq 1 && docker container exec -it $Id /bin/bash -c "lsattr /something.txt; \
  chattr +a /something.txt; \
  lsattr /something.txt; \
  echo hello >> /something.txt; \
  cat /something.txt; \
  echo hello > /something.txt; \
  chattr -a /something.txt"

########### A ###########
# When it is set, the atime of the file, won't be modified, even if it is accesed with the cat command for instance. We have to access it using the editor.
# help: https://superuser.com/questions/464290/why-is-cat-not-changing-the-access-time
# In `/proc/mounts`, is where is relatime or noatime is set. man mount can be helpful in this case, looking for the property relatime.
test $1 -eq 2 && docker container exec -it $Id /bin/bash -c "lsattr /something.txt; \
  stat /something.txt; \
  echo 'press Ctrl+x to exit'; \
  sleep 5s; \
  nano /something.txt; \
  stat /something.txt; \
  chattr +A /something.txt; \
  echo 'press Ctrl+x to exit'; \
  sleep 5s; \
  nano /something.txt; \
  stat /something.txt; \
  chattr -A /something.txt"

########### c ###########
# It compress the file before writing to disk. When reading it uncompress it.

########### C ###########
# It won't be subject to copy-on-write updates.

########### d ###########
# It is not set to be a candidate for backup

########### D ###########
# When a directory with the 'D' attribute set is modified, the changes are written to the disk synchronously to the disk.
# This is equivalent to the 'dirsync' mount option applied to a subset of files.

########### e ###########
# the 'e' attribute indicates that a file is using extents for mapping the block on disk.
# When a directory with the 'D' attribute set is modified, the changes are written to the disk synchronously to the disk.

########### i ###########
# This stands for immutable, and it can be only set or unset by a root capable with CAP_LINUX_IMMUTABLE.
# the file can't:
# - be modified
# - moved
# - be linked
# - most of the file metadata can't be modified (chmod)
# - cannot be open in write mode.
test $1 -eq 3 && docker container exec -it $Id /bin/bash -c "chattr +i /something.txt; \
  echo bye > /something.txt; \
  mv /something.txt /home/something.txt; \
  rm /something.txt; \
  ln -s /something.txt /home/something.txt; \
  chmod 0755 /something.txt; \
  echo byte >> /something.txt; \
  chattr -i /something.txt;
  "

docker container stop $Id
