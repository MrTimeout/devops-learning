#!/bin/bash
#
# Here we are learning how images tags and IDs works.
#
docker image build --tag my:0.1 --file ./Dockerfile --force-rm --no-cache . > /tmp/log
docker image build --tag my:latest --file ./Dockerfile --force-rm --no-cache . >> /tmp/log

function look_for_parent_images {
  if [[ "$#" -ne 1 ]]; then
    echo "Exiting because wrong number of parameters passed"
    exit 1
  fi
  parent="$1"

  while [[ -n $parent ]]; do
    result=$(docker image inspect $parent | jq '.[] | { "Id": .Id, "Parent": .Parent }')
    echo $result
    parent=$(jq -r '.Parent' <<< $result | cut -d':' -f2)
  done
}

look_for_parent_images my:0.1
look_for_parent_images my:latest
