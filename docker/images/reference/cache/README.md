# Understanding simple cache use in Docker

This is a simple example to visualize how docker cache works. At first glance, we are
building the docker image from a _Dockerfile_ and everything is looking fine.

Each step, we are creating a temporary container to execute our "Commands" and removing after
it. For instance, in step 2, we are creating container with ID `6ec31522522c` and removing after it.

In step 4/5, we are running an `echo` command, to notice that after this build, the echo will be cached and not executed.

```sh
> docker build -f ./Dockerfile --tag echo:1.0.0 .
Sending build context to Docker daemon  2.048kB
Step 1/5 : FROM alpine:latest
 ---> 0ac33e5f5afa
Step 2/5 : LABEL maintainer estonoesmiputocorreo@gmail.com
 ---> Running in 6ec31522522c
Removing intermediate container 6ec31522522c
 ---> 7576e9e7041d # This is the intermediate image that we created at each step.
Step 3/5 : LABEL env local-testing
 ---> Running in b3072a37764f
Removing intermediate container b3072a37764f
 ---> 629b914c831d
Step 4/5 : RUN echo "Hello world"
 ---> Running in b5719edbe503
Hello world
Removing intermediate container b5719edbe503
 ---> df4c05bad857
Step 5/5 : CMD ["/usr/bin/env", "bash"]
 ---> Running in b89e524d87a0
Removing intermediate container b89e524d87a0
 ---> 5359fa11c909
Successfully built 5359fa11c909
Successfully tagged echo:1.0.0
```

Notice how in this step, we are not outputing anything to the log like in the previous log.
We are also using the same intermediary images to not create the same images again and again.
This is an awesome feature that can help us to skip some steps when creating the images.
For instance, if we have to install a lot of packages, we could put the step first of all, or nearly
to the beginning, to cache that step, and don't do it every time we build another image.

```sh
Sending build context to Docker daemon  4.096kB
Step 1/5 : FROM alpine:latest
 ---> 0ac33e5f5afa
Step 2/5 : LABEL maintainer estonoesmiputocorreo@gmail.com
 ---> Using cache
 ---> 7576e9e7041d
Step 3/5 : LABEL env local-testing
 ---> Using cache
 ---> 629b914c831d
Step 4/5 : RUN echo "Hello world"
 ---> Using cache
 ---> df4c05bad857
Step 5/5 : CMD ["/usr/bin/env", "bash"]
 ---> Using cache
 ---> 5359fa11c909
Successfully built 5359fa11c909
Successfully tagged echo:1.0.0
```
