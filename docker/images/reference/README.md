# Dockerfile reference

## Usage

### Context

The docker build command builds an image from a _Dockerfile_ and a _context_.

The build's context is a set of files at a specified location `PATH` or `URL`.

Example: `docker build .` or `docker build /usr/local/share/my_container_declaration` or
`docker build https://github.com/{UserName}/RepoName`

To use a file in the build context, the _Dockerfile_ refers to the file specified in an instruction,
for example, a `COPY` instruction. To increase the build's performance, exclude files and directories
by adding a `.dockerignore` file to the context directory.

### Specifying another Dockerfile not in the context

We can specify another _Dockerfile_ that is not in the same context using the `-f` flag:
`docker build -f /path/to/the/Dockerfile .`

### Specifying a repo or/and tag

We can add one or more `repository/tag:version` to the build, so they are stored in the following
repository with the tag and version. Example:
`docker build -t myRepo/myapp:1.0.0 -t myRepo/myapp:latest .`

### How docker daemon creates images

When we send the _Dockerfile_ to the _docker daemon_, it first checks if the file is valid.

The _Docker daemon_ runs the instructions in the _Dockerfile_ one-by-one, committing the result
of each instruction to a new image if necessary, before finally outputting the ID of your new image.
The _Docker daemon_ will automatically clean up the context you sent.

Each instruction is run independently and causes a new image to be created. So when executing `RUN cd /tmp`
we are not persisting the current workdir for the next operation.

Docker uses a build-cache to accelerate the _docker build_ process significantly.

## Syntax

The instructions are not case-sensitive. However, we use upper-case convention to easily differentiate
from standard commands.

First of all, we have to use the instruction `FROM`. This may be after:

- _parser directives_
- _comments_
- _globally scoped ARGs_

### Comments

Comments in the _Dockerfile_ are allowed using the character '#'. They are removed before
parsing the _Dockerfile_. For instance, this piece of code should work:

```Dockerfile
RUN echo Hello \
# something here
world
```

### Leading whitespaces at beginning of the line

Leading whitespaces at the beginning of the comment or instruction are ignored, but discouraged.
For instance, this two examples are the same:

```Dockerfile
  # My comment
  RUN echo "Hello world"

# Same as
# My comment
RUN echo "Hello world"
```

We can use line feeds in commands like that:

```Dockerfile
RUN echo "\
    hello\
    world"
```

### Syntax of directives

The must be at the top before any comment or instruction. They should be in lowercase, but it is not mandatory.
Its syntax is `key=value` form.

#### syntax directive

Available when using the _BuildKit_ backend.

Fix me: we have to add more info about this topic and buildKit...

`# syntax=[remote image reference]`

Examples:

```Dockerfile
# syntax=docker/dockerfile:1
# syntax=docker.io/docker/dockerfile:1
# syntax=example.com/user/repo:tag@sha256:abcdef...
```

#### escape directive

The default escape character is '\'

```Dockerfile
# escape=\
# escape=~
```

Not that regardless of whether the escape parser directive is included in a _Dockerfile_, escaping
is not performed in a _RUN_ command, except at the end of a line.

'\`' is preferable to be used on windows.

## Environment Replacement

We can declare environment variables using the _ENV_ statement. They can be accessed using the dolar sign: `$foo` or `${foo}`

Environment replacement also supports some functionalities of bash:

- `${foo:-baa}` indicates that if foo is not set, it will be replaced by the word baa.
- `${foo:+baa}` indicates that if the foo is set, then the result is baa.

We can space dolar sign, so _Docker daemon_ doesn't interpret it as a environment replacement:

```Dockerfile
FROM alpine:latest
LABEL maintainer estonoesmiputocorreo@gmail.com
ENV FOO=something
ECHO "The variable \$FOO has the value: $FOO"
ECHO "The variable \$BAA doesnt have value so, by default is: ${BAA:-other}"
```

We can use env variables with:

- ADD
- COPY
- ENV
- EXPOSE
- FROM
- LABEL
- STOPSIGNAL
- USER
- VOLUME
- WORKDIR
- ONBUILD (when combined with one of the supported instructions above)

In each instruction, the environment variable will use the same value, for instance:

```Dockerfile
ENV abc=first
ENV abc=second def=$abc # The value of $def will be 'first'
ENV ghi=$abc # The value of ghi will be 'second'
```

## FROM


## RUN

`RUN` has 2 forms:

- `RUN <command>` (shell form, the command is run in a shell, which by default is `/bin/sh -c` on linux or `cmd /S /C` on windows).
- `RUN ["executable", "param1", "param2"]` (exec form).

The `exec` form makes it possible to avoid _shell string munging_, and to `RUN` commands using a base image that does not contain the specified shell executable.

The default shell for the shell form can be changed using the `SHELL` command.

Some example:

```Dockerfile
# Shell form
RUN echo "Hello world"
RUN /bin/bash -c "echo hello"
RUN /bin/bash -c "echo hello; \
echo $HOME"

# Exec form
RUN ["/bin/bash", "-c", "echo hello"] # we can't use single quotes, because we are parsing a JSON.
```

## Links

- [BuildKit](https://github.com/moby/buildkit/blob/master/frontend/dockerfile/docs/syntax.md)
