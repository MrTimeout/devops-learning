#!/bin/bash
#
# Here we are going to test how our Dockerfile building is going to fail.
#
# We are trying to pass a file to the daemon without build context

docker image build -<<EOF
  FROM alpine:latest

  COPY ./init.sh /init.sh

  RUN chmod 0744 /init.sh
EOF
