#!/bin/bash
#
# Using Here document to pass Dockerfile syntax information to daemon.
#

# Without build context with dockerfile passed from STDIN.
docker image build -<<EOF
  FROM alpine:latest
  RUN echo "Hello world"
EOF

# Trying with build context with dockerfile passed from STDIN.
# This file is in ./init_1_failing.sh

# With build context passing file from STDIN.
docker image build -f- . <<EOF
  FROM alpine:latest

  COPY ./entrypoint.sh /entrypoint.sh

  RUN chmod 0744 /entrypoint.sh

  ENTRYPOINT ["/entrypoint.sh"]
EOF

# With build context from URL passing file as STDIN.
docker image build -f- . https://github.com/docker-library/hello-world.git <<EOF
  FROM busybox
  COPY hello.c ./
EOF
