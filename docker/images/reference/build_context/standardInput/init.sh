#!/bin/bash
#
# How to build this Dockerfile?
#
# There are various options:
#   - From STDIN (without build context, it will ignore any -f/--file option)
#   - From URL
#   - From tar

if [[ "$1" == "stdin" ]]; then
  # From STDIN
  docker image build - < Dockerfile

  # From STDIN we are ignoring the option -f/--file
  docker build --file some/big/deep/folder/Dockerfile - < Dockerfile

  # From PIPE
  cat Dockerfile | docker image build -

  # FROM PIPE using echo
  echo -e 'FROM alpine:latest\nRUN echo "Hello world"' | docker image build -

fi

if [[ "$1" == "url" ]]; then
  # From URL without context
  docker image build https://codeberg.org/MrTimeout/devops-learning/raw/branch/main/docker/images/reference/build_context/standardInput/Dockerfile
fi

if [[ "$1" == "tar" ]]; then
  # From tar hosted inside a URL. We are using three compression formats:
  #   - gzip
  #   - bzip2
  #   - xz
  docker image build --tag nginx:testing --file ./Dockerfile_nginx --force-rm .

  docker container run --detach --name webserver --rm --publish 3000:3000 nginx:testing

  # First try with gzip
  imageId=$(docker image build http://127.0.0.1:3000/DockerContext.tar.gz | tail -n 1 | cut -f3 -d' ')
  docker container run --rm -i $imageId

  # Here we are using bzip2
  imageId=$(docker image build http://127.0.0.1:3000/DockerContext.tar.bz2 | tail -n 1 | cut -f3 -d' ')
  docker container run --rm -i $imageId

  # Finally, xz
  imageId=$(docker image build http://127.0.0.1:3000/DockerContext.tar.xz | tail -n 1 | cut -f3 -d' ')
  docker container run --rm -i $imageId

  docker container rm --force webserver
  docker rmi nginx:testing
fi

docker image prune --force
