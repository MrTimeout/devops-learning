#!/bin/bash
#
# The VOLUME directive allows containers to bypass the CoW filesystem. 
# During builds, containers will not maintain volume content because the commit action will just transform container content into images,
# and volumes are not found inside containers.
#
imageId=$(docker image build --file ./Dockerfile --force-rm . | tail -n 1 | cut -f3 -d' ')

# This container will not display the bye.txt file
docker container run --rm -i $imageId ls -al /data
