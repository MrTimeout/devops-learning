#!/bin/bash
#
# When we have two Dockerfiles in the same directory, we can choose which one to use.
docker image build --no-cache --file ./Dockerfile --tag alpine:first --force-rm .
docker image build --no-cache -f ./Dockerfile_second --tag alpine:second --force-rm .

docker image ls alpine:!latest --format="{{.ID}}"
# Just playing with pipes jeje. We can also use rmi which stands for image rm
docker image rm $(docker image ls --all --no-trunc alpine | grep -v latest | tail -n 2 | cut -d ' ' -f1 | cut -d ':' -f2)

# I think that the best practice is to use only one Dockerfile and then put there all the stages and different images to build.
# This property is useful when we want to use a Dockerfile of another directory and use the actual context.
# We can also change the context, so... It is up to you which one to choose.
