#!/bin/bash

for x in "$@"
do
  echo "curling to $x"
  curl -vvvL -X GET --header "Accept: */*" --header "Authorization: basic" $x
done
