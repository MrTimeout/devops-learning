#!/bin/bash
#
# What happens when a build of a docker image fails?
# It depends...
# If the building fails when executing an instruction that needs to create another container to run like a RUN, COPY, ADD, etc.
# we can watch to this container in our list of non-running containers. Don't you believe me? Come with me.

docker image build --file ./Dockerfile --tag alpine:test . >> /tmp/alpine_build.log

# This is the last successfully image. This is just to clarify.
imageId=$(cat /tmp/alpine_build.log | tail -n 5 | egrep -e "\-\-> [a-f0-9]+" | cut -d ' ' -f3)
containerId=$(cat /tmp/alpine_build.log | tail -n 5 | egrep -e "\-\-> .* [a-f0-9]+" | awk '{print $NF}')

echo "Last successful image is $imageId and last container is the one with errors $containerId"

docker container ls --all --filter="id=$containerId"

# When we create an image and something fails, there are , AFAI, one container that will persist on the system, when the last instruction is RUN, COPY or similars.
# If we want to clean up the containers that we create when building the docker image, we can use the option "--force-rm"

# Clean up
docker container rm $containerId > /dev/null 2>&1

## Trying again the same scenario. We are using the option --no-cache because we need to ensure that all the process starts from scratch.
docker image build --force-rm --no-cache --file ./Dockerfile --tag alpine:test . >> /tmp/alpine_build.log
containerId=$(cat /tmp/alpine_build.log | tail -n 1 | awk '{print $NF}')
docker container ls --all --filter="id=$containerId"

echo "We have just created a log file in /tmp/alpine_build.log, if you want to see any of the outputs, you can visualize this file"
