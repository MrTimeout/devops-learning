#!/bin/bash
#
# We can use labels to categorize and group our images.
# For instance, If we want to test an image from dev environment in local environment and we want to add some other label:
# This images are dangling because we are not tagging them with any repository.
docker image build --file ./Dockerfile --label env=local --label other=label --force-rm --no-cache .
docker image build --file ./Dockerfile --label env=local --label another=label --force-rm --no-cache .

# This example is to notice that we can add more than on filter statment
imageId=$(docker image ls --all --filter "label=env=local" --filter "label=other=label" --quiet)
imageId2=$(docker image ls --all --filter "label=another=label" --quiet)

# We can also use: docker image inspect $imageId --format "{{json .Config.Labels}}" | jq .
docker image inspect $imageId | jq '.[].Config.Labels'
docker image inspect $imageId2 | jq '.[].Config.Labels'
