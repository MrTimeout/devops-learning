#!/bin/bash
#
# We can use the argument of 'docker image build' --build-arg to paas a build arg to the contruction of the image.
# For example, with the Dockerfile that we have in this folder, we can add a '--build-arg VERSION=3.12' to download
# the version 3.12 instead of the latest.

docker image build --file ./Dockerfile --tag alpine:testing --build-arg VERSION=3.12 .

# We can also paas other well known variable as:
#   - HTTP_PROXY
#   - HTTPS_PROXY
#   - FTP_PROXY
# more on that later...
