#!/bin/bash
# This script will print all the Read-only images of the final image.
# It goes accross the parent until reach the base image.
#
# Author MrTimeout
#
# GPL 3.0

if [[ $# -ne 1 ]]; then
  echo "Usage: introduce the image that you want to parse, ID or name and the image tree will be outputed to you";
  exit 1;
fi

function inspect_images {
  echo $(docker image inspect $1 | jq .[0].Id | cut -d ':' -f2 | cut -d '"' -f1);
}

image=$1;

all_local_images=$(docker image ls --all --no-trunc --format="{{.ID}} {{.Repository}}:{{.Tag}}")

if [[ $(grep -c "$image" <<< "$all_local_images") -eq 0 ]]; then
  echo "The ID or Repository:Tag is not valid or hasn't be found";
  exit 2
fi

parent=$(inspect_images $image);

while [[ -n $parent ]]; do
  echo $parent;
  parent=$(inspect_images $parent);
done
