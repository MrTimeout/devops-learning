#!/bin/bash

mkdir --parent /etc/ssl/certs
mkdir --parent --mode=700 /etc/ssl/private

# Explanation:
#   req: we want to use X.509 Certificate Signing Request (CSR). It is a Public Key Infrastructure(PKI)
#   x509: This is used to tell the command that we want to use self-signed certificate.
#   nodes: Skip the option to secure the certificate with a passphrase. It is needed because nginx needs to read the
#       file without passphrase, otherwise we will have to write the passphrase every time the service reboots.
#   days 365: Time that the certificate will be valid.
#   newkey rsa:4096: Tells to create a new key and at the same time the certificate. We are creating a pair.
#   keyout: Where the private key goes
#   out: where the certificate goes
openssl req -x509 -nodes \
  -days 365 \
  -newkey rsa:4096 \
  -subj "/C=ES/ST=Galicia/L=Ourense/O=home Name/OU=Org/CN=localhost" \
  -keyout /etc/ssl/private/nginx.key \
  -out /etc/ssl/certs/nginx.crt
