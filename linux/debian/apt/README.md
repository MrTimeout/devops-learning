# APT learning

APT means Advanced Package Tool and it is a set of tools for managing Debian packages.

## sources.list file

`/etc/apt/sources.list` is a file with the following sections:

### Archive type

The first entry on each line:

- _deb_: it means the repository in the URL provided contains pre-compiled packages.
- _dev-src_: it means source packages with _Debian Control File_(.dsc) and the _diff.gz_ containing the changes needed for packaging the program.

### Repository URL

URL of the repository where the packages will be downloaded from. We can find them in [Debian Worldwide sources.list mirrors](https://www.debian.org/mirror/list).

### Distribution

It can be either the _release code name_ / alias:

- _jessie_
- _stretch_
- _buster_
- _sid_

or the _release class_:

- _old stable_
- _stable_
- _testing_
- _unstable_

respectively.

### Component

There are normally three components which can be used on Debian, namely:

- _main_: This contains packages that are part of Debian distribution. These packages are _DFSG_(Debian Free Software Guidelines) compliant.
- _contrib_: The packages here are _DFSG_ compliant, but contains packages which are not in the main repository.
- _non-free_: This contains software packages which do not comply with the _DFSG_.

### Example

If we need to use https to secure our connections, we will have to install the package `apt-transport-https` when debian version is 9 or older.

If we need the Backports, contrib and non-free components, we can add `{ditribution}-backports` to the sources.list

```
deb http://deb.debian.org/debian buster main contrib non-free
deb-src http://deb.debian.org/debian buster main contrib non-free

# With backports
deb http://deb.debian.org/debian buster-backports main contrib non-free
deb-src http://deb.debian.org/debian buster-backports main contrib non-free

# Template
# [deb|deb-src] {repositoryUrl} ([jessie|stretch|buster|sid]|[old stable|stable|testing|unstable]) main contrib non-free
```

In case of errors updating using the command `apt-get update` it is better to use the command `apt` for interactive use.

If that doesn't fix the issue then second option is to use `apt-get update --allow-releaseinfo-change`

## How to add another repository

We can create a new entry inside `/etc/apt/sources.list.d/` and add the following line:

`deb [arch=amd64] https://download.docker.com/linux/debian buster stable` for instance.

Another way of doing that is:

```
> sudo apt-get install software-properties-common

# $(lsb_release --short --codename) is the same as $(lsb_relase -cs)
> sudo add-apt-repository \
  "deb [arch=amd64] https://download.docker.com/linux/debian \
  $(lsb_release -cs) \
  stable

> sudo apt-get update
```

It won't work because we need the GPG(GNU Privacy Guard) keys of the docker repository (in this case).

## Importing apt keys

`apt-key adv --keyserver {server-address} --recv-keys {key-id}`

or to install gpg key, you can do that:

`curl -sSfL https://download.docker.com/linux/debian/gpg | sudo apt-key add -`

The curl will return just the public key.

How to test if if was added correctly: `apt-key fingerprint {fingerprint_of_key}` it is located at `/etc/apt/trusted.gpg` and you can access it using `gpg /etc/apt/trusted.gpg`

TODO: we have to add more info here because apt-key is deprected so we need to learn how to do all that stuff, following the best practises.


