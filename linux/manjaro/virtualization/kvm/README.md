# How to install kvm

```sh
sudo pacman -Syy
sudo pacman -S archlinux-keyring
sudo pacman -S qemu virt-manager virt-viewer dnsmasq vde2 bridge-utils openbsd-netcat
sudo pacman -S iptables ebtables
sudo pacman -S libguestfs
sudo systemctl status libvirtd.service
sudo systemctl start libvirtd.service
# This two lines are not tested, the main idea is uncomment them and all is going to work just fine.
sudo sed 's/#\(unix_sock_group = "libvirt"\)/\1/g' /etc/libvirt/libvirt.conf
sudo sed 's/#\(unix_sock_rw_perms = "0770"\)/\1/g' /etc/libvirt/libvirt.conf
sudo usermod -aG libvirt $(whoami)
sudo systemctl restart libvirtd.service
```
